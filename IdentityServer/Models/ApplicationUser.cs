using AspNetCore.Identity.Mongo.Model;

namespace IdentityServer.Models
{
    public class ApplicationUser : MongoUser
    {
        public object GlobalUid { get; internal set; }
    }

    public class ApplicationRole : MongoRole
    {
    }
}
