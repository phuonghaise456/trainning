using System;
using System.Collections.Generic;
using IdentityServer4;
using IdentityServer4.Models;

namespace IdentityServer
{
    public static class Config
    {
        public static IEnumerable<IdentityResource> GetIdentityResources()
        {
            return new List<IdentityResource>
            {
                new IdentityResources.OpenId(),
                new IdentityResources.Profile(),
            };
        }

        public static IEnumerable<ApiResource> GetApis()
        {
            return new List<ApiResource>
            {
                new ApiResource("book", "Book API")
                {
                    Scopes = { "book.read", "book.write", "book.delete", "category.read" }
                }
                //new ApiResource("category", "category API")
                //{
                //    Scopes = { "category.read"}
                //},
            };
        }

        public static IEnumerable<Client> GetClients()
        {
            return new List<Client>
            {
                new Client
                {
                    ClientId = "client_id_mvc",
                    ClientSecrets = {new Secret("client_secret_mvc".Sha256())},
                    AllowedGrantTypes = GrantTypes.Code,
                    //RefreshTokenExpiration = TokenExpiration.Sliding,

                    AccessTokenLifetime = (int) new TimeSpan(0,1,0).TotalSeconds,

                    RefreshTokenUsage = TokenUsage.OneTimeOnly,

                    AllowedScopes = { "book.read","book.write","book.delete", "category.read",
                        IdentityServerConstants.StandardScopes.OpenId,
                        IdentityServerConstants.StandardScopes.Profile,
                    },
                    //redirect to after login
                    RedirectUris = {"https://localhost:6001/signin-oidc" },

                    //redirect to after logout
                    PostLogoutRedirectUris = {"https://localhost:6001/signout-callback-oidc" },

                    AllowOfflineAccess = true,
                    RequireConsent = false
                },
                new Client
                {
                    ClientId = "client_credentials_id",
                    ClientSecrets = {new Secret("client_credentials_secret".Sha256())},
                    AllowedGrantTypes = GrantTypes.ClientCredentials,
                    AllowedScopes = { "book.read","category.read" }
                },
                new Client
                {
                    ClientId = "client_password_id",
                    ClientSecrets = {new Secret("client_password_secret".Sha256())},
                    AllowedGrantTypes = GrantTypes.ResourceOwnerPassword,
                    AllowedScopes = { "book.read","category.read" },
                    AllowOfflineAccess = true
                }
            };
        }

        public static IEnumerable<ApiScope> GetApiScopes()
        {
            return new List<ApiScope>
            {
                new ApiScope("book.read", "Access to WebApi"),
                new ApiScope("book.write", "Access to WebApi"),
                new ApiScope("book.delete", "Access to WebApi"),
                new ApiScope("category.read", "Access to WebApi")
            };
        }
    }
}
