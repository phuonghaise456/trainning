using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace TrainingMVC.Models
{
    public class Book
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }

        [BsonElement("Name")]
        public string BookName { get; set; }

        [BsonElement]
        public decimal Price { get; set; }

        [BsonElement]
        public string Category { get; set; }

        [BsonElement]
        public string Author { get; set; }

    }
}
