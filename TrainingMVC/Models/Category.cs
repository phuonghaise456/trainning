using System;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace TrainingMVC.Models
{
    public class Category
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }

        [BsonElement("Name")]
        public string CategoryName { get; set; }
    }
}
