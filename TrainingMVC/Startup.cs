using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using TrainingMVC.Services;
using TrainingMVC.Utils;
using MediatR;
using System;
using Microsoft.AspNetCore.Authentication;

namespace TrainingMVC
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMemoryCache();
            services.AddSession();

            services.AddAuthentication(config =>
            {
                config.DefaultScheme = "Cookie";
                config.DefaultChallengeScheme = "oidc";
            })
            .AddCookie("Cookie", config =>
            {
                // Configure the client application to use sliding sessions          
                //config.SlidingExpiration = true;
                // Expire the session of 15 minutes of inactivity
                //config.ExpireTimeSpan = TimeSpan.FromMinutes(15);

                config.Events.OnSigningOut = async e =>
                {
                    // revoke refresh token on sign-out
                    await e.HttpContext.RevokeUserRefreshTokenAsync();
                };
            }).AddOpenIdConnect("oidc", config =>
            {

                //Event fire when received cookie ticket
                config.Events.OnTicketReceived = async (context) =>
                {
                    //default lifetime is 14 days
                    //context.Properties.ExpiresUtc = DateTime.UtcNow.AddHours(1);
                };

                config.Authority = "https://localhost:7001";
                config.RequireHttpsMetadata = false;

                config.ClientId = Configuration["ClientInfoSettings:ClientId"];
                config.ClientSecret = Configuration["ClientInfoSettings:ClientSecret"];
                config.ResponseType = Configuration["ClientInfoSettings:ResponseType"];

                config.SaveTokens = true; // persist token in cookie

                config.Scope.Clear();
                config.Scope.Add("openid");
                config.Scope.Add("profile");
                config.Scope.Add("offline_access");

                config.Scope.Add("book.read");
                config.Scope.Add("book.write");
                config.Scope.Add("book.delete");
                config.Scope.Add("category.read");
            });

            services.AddAccessTokenManagement();

            services.AddUserAccessTokenClient("BookApi", configureClient: client =>
            {
                client.BaseAddress = new Uri("https://localhost:5001");
            });

            services.AddAutoMapper(typeof(Startup));
            services.AddMvc().AddNToastNotifyToastr();
            services.AddMediatR(typeof(Startup));
            services.AddTransient<DbContext>();
            services.AddTransient<IBookService, BookService>();
            services.AddTransient<ICategoryService, CategoryService>();
            services.AddTransient<IAuthenticationUserService, AuthenticationUserService>();

            services.AddControllersWithViews();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                app.UseHsts();
            }
            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseSession();

            app.UseRouting();
            app.UseAuthentication();
            app.UseAuthorization();
            app.UseNToastNotify();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Book}/{action=Index}").RequireAuthorization();
            });
        }
    }
}
