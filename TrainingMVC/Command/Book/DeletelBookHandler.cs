using System;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using TrainingMVC.Services;

namespace TrainingMVC.Command.Book
{
    public class DeletelBookHandler : IRequestHandler<DeleteBookCommand, Object>
    {
        private readonly IBookService _bookService;

        public DeletelBookHandler(IBookService bookService)
        {
            _bookService = bookService;
        }

        public async Task<Object> Handle(DeleteBookCommand request, CancellationToken cancellationToken)
        {
            return await _bookService.Remove(request.Id);
        }


    }
}