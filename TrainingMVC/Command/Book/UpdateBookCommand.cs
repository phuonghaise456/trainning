using System;
using MediatR;
using TrainingMVC.Dtos;

namespace TrainingMVC.Command.Book
{
    public class UpdateBookCommand : IRequest<Object>
    {
        public BookDto BookDto { get; }
        public string Id { get; }

        public UpdateBookCommand(BookDto bookDto, string id)
        {
            this.BookDto = bookDto;
            this.Id = id;
        }
    }
}