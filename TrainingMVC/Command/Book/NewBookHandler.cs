using System;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using TrainingMVC.Services;

namespace TrainingMVC.Command.Book
{
    public class NewBookHandler : IRequestHandler<NewBookCommand, Object>
    {
        private readonly IBookService _bookService;

        public NewBookHandler(IBookService bookService)
        {
            _bookService = bookService;
        }

        public async Task<Object> Handle(NewBookCommand request, CancellationToken cancellationToken)
        {
            return await _bookService.Add(request.BookDto);
        }


    }
}