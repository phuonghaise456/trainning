using System;
using MediatR;

namespace TrainingMVC.Command.Book
{
    public class DeleteBookCommand : IRequest<Object>
    {
        public string Id { get; }
        
        public DeleteBookCommand(string id)
        {
            Id = id;
        }
    }
}