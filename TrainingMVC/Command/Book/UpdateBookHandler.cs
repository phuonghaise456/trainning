using System;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using TrainingMVC.Services;

namespace TrainingMVC.Command.Book
{
    public class UpdateBookHandler : IRequestHandler<UpdateBookCommand, Object>
    {
        private readonly IBookService _bookService;

        public UpdateBookHandler(IBookService bookService)
        {
            _bookService = bookService;
        }

        public async Task<Object> Handle(UpdateBookCommand request, CancellationToken cancellationToken)
        {
            return await _bookService.Update(request.Id, request.BookDto);
        }


    }
}