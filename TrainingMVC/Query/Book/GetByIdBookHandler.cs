using System.Threading;
using System.Threading.Tasks;
using MediatR;
using TrainingMVC.Dtos;
using TrainingMVC.Services;

namespace TrainingMVC.Query.Book
{
    public class GetByIdBookHandler : IRequestHandler<GetByIdBookQuery, BookDto>
    {
        private readonly IBookService _bookService;

        public GetByIdBookHandler(IBookService bookService)
        {
            _bookService = bookService;
        }

        public async Task<BookDto> Handle(GetByIdBookQuery request, CancellationToken cancellationToken)
        {
            var bookDto = await _bookService.GetById(request.Id);
            return bookDto;
        }
    }
}