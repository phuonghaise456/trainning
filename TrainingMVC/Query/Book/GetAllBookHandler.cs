using System;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using TrainingMVC.Services;

namespace TrainingMVC.Query.Book
{
    public class GetAllBookHandler : IRequestHandler<GetAllBookQuery, Object>
    {
        private readonly IBookService _bookService;
        public static int NUM_PER_PAGE = 5;

        public GetAllBookHandler(IBookService bookService)
        {
            _bookService = bookService;
        }

        public async Task<Object> Handle(GetAllBookQuery request, CancellationToken cancellationToken)
        {
            dynamic bookDtos = await _bookService.GetAll(NUM_PER_PAGE, request.Page);

            return bookDtos;
        }
    }
}