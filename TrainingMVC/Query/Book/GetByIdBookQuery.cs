using MediatR;
using TrainingMVC.Dtos;

namespace TrainingMVC.Query.Book
{
    public class GetByIdBookQuery : IRequest<BookDto>
    {
        public string Id { get; }
        
        public GetByIdBookQuery(string id)
        {
            Id = id;
        }
    }
}
