using System;
using MediatR;

namespace TrainingMVC.Query.Book
{
    public class GetAllBookQuery : IRequest<Object>
    {
        public int Page { get; }

        public GetAllBookQuery(int page)
        {
            this.Page = page;
        }
    }
}
