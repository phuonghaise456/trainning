using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using TrainingMVC.Dtos;
using TrainingMVC.Services;

namespace TrainingMVC.Query.Category
{
    public class GetAllCategoryHandler : IRequestHandler<GetAllCategoryQuery, List<CategoryDto>>
    {
        private readonly ICategoryService _categoryService;

        public GetAllCategoryHandler(ICategoryService categoryService)
        {
            _categoryService = categoryService;
        }

        public async Task<List<CategoryDto>> Handle(GetAllCategoryQuery request, CancellationToken cancellationToken)
        {
            var categoryDto = await _categoryService.GetAll();
            return categoryDto;
        }
    }
}