using System.Collections.Generic;
using MediatR;
using TrainingMVC.Dtos;

namespace TrainingMVC.Query.Category
{
    public class GetAllCategoryQuery : IRequest<List<CategoryDto>>
    {
    }
}
