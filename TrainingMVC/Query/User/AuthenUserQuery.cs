using MediatR;
using TrainingMVC.Dtos;

namespace TrainingMVC.Query.User
{
    public class AuthenUserQuery : IRequest<bool>
    {
        public UserDto UserDto { get; }

        public AuthenUserQuery(UserDto userDto)
        {
            UserDto = userDto;
        }
    }
}
