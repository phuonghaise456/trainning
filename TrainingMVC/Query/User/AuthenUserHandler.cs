using System.Threading;
using System.Threading.Tasks;
using MediatR;
using TrainingMVC.Services;

namespace TrainingMVC.Query.User
{
    public class AuthenUserHandler : IRequestHandler<AuthenUserQuery, bool>
    {
        private readonly IAuthenticationUserService _authenticationUserService;

        public AuthenUserHandler(IAuthenticationUserService authenticationUserService)
        {
            _authenticationUserService = authenticationUserService;
        }

        public async Task<bool> Handle(AuthenUserQuery request, CancellationToken cancellationToken)
        {
            return await _authenticationUserService.AuthenticationUser(request.UserDto);
        }
    }
}