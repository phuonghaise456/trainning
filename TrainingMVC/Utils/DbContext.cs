using Microsoft.Extensions.Configuration;
using MongoDB.Driver;
using TrainingMVC.Models;

namespace TrainingMVC.Utils
{
    public class DbContext
    {
        public IMongoCollection<Book> Books { get; set; }
        public IMongoCollection<Category> Categories { get; set; }

        public DbContext(IConfiguration settings)
        {
            //var client = new MongoClient(option.ConnectionString);
            //var database = client.GetDatabase(option.DatabaseName);
            //_books = database.GetCollection<Book>(option.BooksCollectionName);

            var client = new MongoClient(settings["BookstoreDatabaseSettings:ConnectionString"]);
            var database = client.GetDatabase(settings["BookstoreDatabaseSettings:DatabaseName"]);

            Books = database.GetCollection<Book>(settings["BookstoreDatabaseSettings:BooksCollectionName"]);
            Categories = database.GetCollection<Category>(settings["BookstoreDatabaseSettings:CategoriesCollectionName"]);
        }
    }
}
