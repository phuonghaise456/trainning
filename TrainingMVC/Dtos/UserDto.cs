using System;
using System.ComponentModel.DataAnnotations;

namespace TrainingMVC.Dtos
{
    public class UserDto
    {
        [Required]
        [Display(Name = "User Name")]
        public string UserName { get; set; }

        [Required]
        public string Password { get; set; }
    }
}
