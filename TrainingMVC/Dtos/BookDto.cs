using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace TrainingMVC.Dtos
{
    public class BookDto
    {
        public string Id { get; set; }

        [Required]
        [Display(Name = "Book Name")]
        public string BookName { get; set; }


        [Range(1, 1000000)]
        [DataType(DataType.Currency)]
        [Column(TypeName = "decimal(18, 2)")]
        public decimal Price { get; set; }

        [Required]
        public string Category { get; set; }

        [Required]
        public string Author { get; set; }
    }
}
