using System;
namespace TrainingMVC.Dtos
{
    public class CategoryDto
    {
        public string Id { get; set; }

        public string CategoryName { get; set; }
    }
}
