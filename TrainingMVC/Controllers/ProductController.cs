using System;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace TrainingMVC.Controllers
{
    public class ProductController : Controller
    {
        private readonly ILogger<ProductController> _logger;

        public ProductController(ILogger<ProductController> logger)
        {
            _logger = logger;
        }

        [HttpGet]
        public IActionResult Products()
        {
            //Generate test products data
            var products = Enumerable.Range(1, 1000).Select(i =>
                 new Product() { Id = i, BrandId = i % 10 + 1, CategoryId = i % 20 + 1, ExpirationDate = "12/" + (i % 31 + 1) + "/2020", Name = "Product" + i, OriginCity = "City" + (i % 40 + 1), OriginCountry = "Country" + (i % 50 + 1), ProducerName = "Producer" + i + 100 + 1, ProductionDate = "1/" + (i % 31 + 1) + "/2020" }
            ).ToList();
            return View(products);
        }
    }

    public class Product
    {
        public int Id { get; set; }
        public int CategoryId { get; set; }
        public int BrandId { get; set; }
        public string Name { get; set; }
        public string ProducerName { get; set; }
        public string OriginCity { get; set; }
        public string OriginCountry { get; set; }
        public string ProductionDate { get; set; }
        public string ExpirationDate { get; set; }
    }

}
