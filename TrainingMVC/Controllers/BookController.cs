using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Net.Http;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json.Linq;
using NToastNotify;
using TrainingMVC.Command.Book;
using TrainingMVC.Dtos;
using TrainingMVC.Query.Book;
using TrainingMVC.Query.Category;

namespace TrainingMVC.Controllers
{
    public class BookController : Controller
    {
        private IToastNotification _toastNotification;
        private IMediator _mediator;
        private IHttpClientFactory _httpClientFactory;
        private IConfiguration _configuration;

        public BookController(IConfiguration configuration, IMediator mediator, IToastNotification toastNotification, IHttpClientFactory httpClientFactory)
        {
            _mediator = mediator;
            _toastNotification = toastNotification;
            _httpClientFactory = httpClientFactory;
            _configuration = configuration;
        }

        [HttpGet]
        public async Task<IActionResult> Index(int? page)
        {
            await CheckValidAccessToken();




            




            dynamic bookDtos = await _mediator.Send(new GetAllBookQuery(page ?? 1));
            if (((List<BookDto>)bookDtos.books).Count == 0)
            {
                bookDtos = await _mediator.Send(new GetAllBookQuery((int)--page));

            }
            ViewBag.totalPage = (int)bookDtos.totalPages;
            ViewBag.currentPage = page ?? 1;

            return View((List<BookDto>)bookDtos.books);
        }

        [HttpGet]
        public async Task<IActionResult> Insert()
        {
            await CheckValidAccessToken();

            var accessToken = await GetAccessToken();
            var categoryDtos = await _mediator.Send(new GetAllCategoryQuery());
            var tuple = new Tuple<List<CategoryDto>, BookDto>(categoryDtos, new BookDto());
            return View(tuple);
        }

        [HttpPost]
        public async Task<IActionResult> Insert([Bind(Prefix = "Item2")] BookDto bookDto)
        {
            await CheckValidAccessToken();
            switch (ModelState.IsValid)
            {
                case true:
                    var response = await _mediator.Send(new NewBookCommand(bookDto));
                    _toastNotification.AddSuccessToastMessage("Added Book");
                    return RedirectToAction(nameof(Index));
                case false:
                    var categoryDtos = await _mediator.Send(new GetAllCategoryQuery());
                    var tuple = new Tuple<List<CategoryDto>, BookDto>(categoryDtos, new BookDto());
                    return View(tuple);
            }
        }

        [HttpGet]
        public async Task<IActionResult> Update(string id)
        {
            await CheckValidAccessToken();
            var bookDto = await _mediator.Send(new GetByIdBookQuery(id));
            if (bookDto == null)
            {
                return NotFound();
            }

            var categoryDtos = await _mediator.Send(new GetAllCategoryQuery());
            var tuple = new Tuple<List<CategoryDto>, BookDto>(categoryDtos, bookDto);
            return View(tuple);
        }

        [HttpPost]
        public async Task<IActionResult> Update([Bind(Prefix = "Item2")] BookDto bookDto)
        {
            await CheckValidAccessToken();
            switch (ModelState.IsValid)
            {
                case true:
                    var response = await _mediator.Send(new UpdateBookCommand(bookDto, bookDto.Id));
                    _toastNotification.AddSuccessToastMessage("Updated Book");
                    return RedirectToAction("Index");
                case false:
                    var categoryDtos = await _mediator.Send(new GetAllCategoryQuery());
                    var tuple = new Tuple<List<CategoryDto>, BookDto>(categoryDtos, bookDto);
                    return View(tuple);
            }
        }

        [HttpGet]
        public async Task<IActionResult> Delete(int page, string id)
        {
            await CheckValidAccessToken();
            var currentPage = page;
            var response = await _mediator.Send(new DeleteBookCommand(id));
            _toastNotification.AddSuccessToastMessage("Removed Book");
            return RedirectToAction("Index", new { page = currentPage });
        }

        [HttpGet]
        public async Task Logout()
        {
            await HttpContext.SignOutAsync("Cookie");
            await HttpContext.SignOutAsync("oidc");
        }

        public async Task<string> GetIdToken()
        {
            return await HttpContext.GetTokenAsync("id_token");
        }

        public async Task<string> GetAccessToken()
        {
            return await HttpContext.GetTokenAsync("access_token");
        }

        public async Task<string> GetRefreshToken()
        {
            return await HttpContext.GetTokenAsync("refresh_token");
        }

        public async Task CheckValidAccessToken()
        {
            var accessToken = await GetAccessToken();

            var tokenHandler = new JwtSecurityTokenHandler();
            var jwtSecurityToken = tokenHandler.ReadJwtToken(accessToken);

            var validTo = jwtSecurityToken.ValidTo;
            var valid = DateTime.Compare(DateTime.Now, validTo.ToLocalTime());

            if (valid == 1)
                await RefreshToken();
        }

        public async Task RefreshToken()
        {
            var refreshToken = await HttpContext.GetTokenAsync("refresh_token");

            var dict = new Dictionary<string, string>();
            dict.Add("client_secret", _configuration["ClientInfoSettings:ClientSecret"]);
            dict.Add("grant_type", _configuration["ClientInfoSettings:GrantType"]);
            dict.Add("client_id", _configuration["ClientInfoSettings:ClientId"]);
            dict.Add("refresh_token", refreshToken.ToString());
            dict.Add("authorization_code", "");

            var apiClient = _httpClientFactory.CreateClient();
            //apiClient.SetBearerToken(accessToken);

            var response = await apiClient.PostAsync("https://localhost:7001/connect/token", new FormUrlEncodedContent(dict));

            JObject json = JObject.Parse(await response.Content.ReadAsStringAsync());

            var authInfo = await HttpContext.AuthenticateAsync("Cookie");

            authInfo.Properties.UpdateTokenValue("id_token", json["id_token"].ToString());
            authInfo.Properties.UpdateTokenValue("access_token", json["access_token"].ToString());
            authInfo.Properties.UpdateTokenValue("refresh_token", json["refresh_token"].ToString());

            await HttpContext.SignInAsync("Cookie", authInfo.Principal, authInfo.Properties);
        }
    }
}
