using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using TrainingMVC.Dtos;
using Microsoft.AspNetCore.Http;
using NToastNotify;
using MediatR;
using TrainingMVC.Query.User;

namespace TrainingMVC.Controllers
{
    public class LoginController : Controller
    {
        private IMediator _mediator;
        private IToastNotification _toastNotification;
        public LoginController(IToastNotification toastNotification, IMediator mediator)
        {
            _toastNotification = toastNotification;
            _mediator = mediator;
        }

        public IActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Login(UserDto userDto)
        {
            if (ModelState.IsValid)
            {
                var login = await _mediator.Send(new AuthenUserQuery(userDto));
                if (!login)
                {
                    //ModelState.AddModelError("", "The User name or password is incorrect");
                    _toastNotification.AddErrorToastMessage("The User name or password is incorrect");
                    return View();
                }
                HttpContext.Session.SetString("username", userDto.UserName);
                return RedirectToAction("Index", "Book");
            }
            return View();
        }

        [HttpGet]
        public IActionResult Logout()
        {
            HttpContext.Session.Remove("username");
            return RedirectToRoute(new
            {
                controller = "Book",
                action = "Index"
            });
        }
    }
}