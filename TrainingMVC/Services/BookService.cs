using System;
using TrainingMVC.Models;
using System.Threading.Tasks;
using TrainingMVC.Dtos;
using AutoMapper;
using System.Net.Http;
using Newtonsoft.Json;
using System.Text;

namespace TrainingMVC.Services
{
    public class BookService : IBookService
    {
        private readonly IMapper _mapper;
        private readonly IHttpClientFactory _httpClientFactory;

        public BookService(IMapper mapper, IHttpClientFactory httpClientFactory)
        {
            _mapper = mapper;
            _httpClientFactory = httpClientFactory;
        }

        public async Task<Object> GetAll(int pageSize, int page)
        {
            var apiClient = _httpClientFactory.CreateClient("BookApi");
            //apiClient.SetBearerToken(accessToken);

            var response = await apiClient.GetAsync("/api/Books/GetAll?page=" + page + "&pageSize=" + pageSize);
            if (!response.IsSuccessStatusCode) return null;
            var content = await response.Content.ReadAsStringAsync();
            var results = JsonConvert.DeserializeObject<BookResult>(content);

            return results;
        }

        public async Task<BookDto> GetById(string id)
        {
            var apiClient = _httpClientFactory.CreateClient("BookApi");
            //apiClient.SetBearerToken(accessToken);

            var response = await apiClient.GetAsync("/api/Books/GetById/" + id);
            if (!response.IsSuccessStatusCode) return null;
            var content = await response.Content.ReadAsStringAsync();
            var results = JsonConvert.DeserializeObject<Book>(content);

            return _mapper.Map<BookDto>(results);
        }

        public async Task<Object> Add(BookDto bookIn)
        {
            StringContent content = new StringContent(JsonConvert.SerializeObject(_mapper.Map<Book>(bookIn)), Encoding.UTF8, "application/json");

            Console.WriteLine(content.ToString());
            var apiClient = _httpClientFactory.CreateClient("BookApi");
            //apiClient.SetBearerToken(accessToken);

            var response = await apiClient.PostAsync("/api/Books/Add", content);
            if (!response.IsSuccessStatusCode) return null;
            string resultContent = await response.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<Object>(resultContent);
        }

        public async Task<Object> Update(string id, BookDto bookIn)
        {
            //Action<object> act = (object obj) =>
            //{
            //    dynamic arg = obj;
            //    var book = _mapper.Map<Book>((BookDto)arg.book);
            //    var id = (string)arg.ID;
            //    _books.ReplaceOne(b => b.Id == id, book);
            //};
            //Task updateData = new Task(act, new { ID = id, book = bookIn });
            //updateData.Start();
            //await updateData;

            StringContent content = new StringContent(JsonConvert.SerializeObject(_mapper.Map<Book>(bookIn)), Encoding.UTF8, "application/json");

            var apiClient = _httpClientFactory.CreateClient("BookApi");
            //apiClient.SetBearerToken(accessToken);

            var response = await apiClient.PutAsync("/api/Books/Update/" + id, content);
            if (!response.IsSuccessStatusCode) return null;
            string resultContent = await response.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<Object>(resultContent);
        }

        public async Task Remove(BookDto bookIn) { }

        public async Task<Object> Remove(string id)
        {
            var apiClient = _httpClientFactory.CreateClient("BookApi");
            //apiClient.SetBearerToken(accessToken);

            var response = await apiClient.DeleteAsync("/api/Books/Delete/" + id);
            if (!response.IsSuccessStatusCode) return null;
            string resultContent = await response.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<Object>(resultContent);
        }
    }
}