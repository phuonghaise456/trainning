using TrainingMVC.Dtos;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace TrainingMVC.Services
{
    public interface ICategoryService
    {
        Task<List<CategoryDto>> GetAll();

    }
}
