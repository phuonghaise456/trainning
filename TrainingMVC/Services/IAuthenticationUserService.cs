using System.Threading.Tasks;
using TrainingMVC.Dtos;
using TrainingMVC.Models;

namespace TrainingMVC.Services
{
    public interface IAuthenticationUserService
    {
        Task<bool> AuthenticationUser(UserDto userDto);
        //Task<User> AuthenticationUser(string userName, string password);
        //string GenerateJSONWebToken(User userInfo);
        Task<string>  GenerateJSONWebToken();
    }
}
