using TrainingMVC.Dtos;
using System.Collections.Generic;
using System.Threading.Tasks;
using MongoDB.Driver;
using AutoMapper;
using TrainingMVC.Models;
using TrainingMVC.Utils;
using System.Net.Http;
using IdentityModel.Client;

namespace TrainingMVC.Services
{
    public class CategoryService : ICategoryService
    {
        private readonly IMapper _mapper;
        private readonly IHttpClientFactory _httpClientFactory;


        public CategoryService(IMapper mapper, IHttpClientFactory httpClientFactory)
        {
            _mapper = mapper;
            _httpClientFactory = httpClientFactory;
        }
        public async Task<List<CategoryDto>> GetAll()
        {
            var apiClient = _httpClientFactory.CreateClient("BookApi");
            //apiClient.SetBearerToken(accessToken);

            var response = await apiClient.GetAsync("/api/Categories/GetAll");
            var content = await response.Content.ReadAsStringAsync();
            var results = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Category>>(content);

            var categoriesDto = new List<CategoryDto>();

            results.ForEach(c => categoriesDto.Add(_mapper.Map<CategoryDto>(c)));

            return categoriesDto;
        }
    }
}
