using TrainingMVC.Dtos;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace TrainingMVC.Services
{
    public interface IBookService
    {
        Task<object> GetAll(int pageSize, int page);

        Task<BookDto> GetById(string id);

        Task<object> Add(BookDto book);

        Task<object> Update(string id, BookDto bookIn);

        Task Remove(BookDto bookIn);

        Task<object> Remove(string id);

    }
}
