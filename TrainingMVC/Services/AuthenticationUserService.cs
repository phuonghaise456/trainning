using System;
using System.Collections.Generic;
using System.Linq;
using TrainingMVC.Models;
using System.IdentityModel.Tokens.Jwt;
using System.Text;
using Microsoft.IdentityModel.Tokens;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using TrainingMVC.Dtos;

namespace TrainingMVC.Services
{
    public class AuthenticationUserService : IAuthenticationUserService
    {
        /// IConfiguration define in appsetting.json
        private IConfiguration _config;
        private List<User> _listUser = new List<User>()
        {
            new User {UserName = "user1", Password = "111"},
            new User {UserName = "user2", Password = "222"}
        };

        public AuthenticationUserService(IConfiguration config) => _config = config;

        public async Task<bool> AuthenticationUser(UserDto userDto)
        {
            Func<object, bool> fun = (object obj) =>
             {
                 dynamic arg = obj;
                 var user = _listUser.SingleOrDefault(u => u.UserName == arg.USERNAME && u.Password == arg.PASSWORD);
                 if (user == null) return false;
                 return true;
             };
            Task<bool> checkUser = new Task<bool>(fun, new { USERNAME = userDto.UserName, PASSWORD = userDto.Password });
            checkUser.Start();
            await checkUser;
            return checkUser.Result;
        }

        //public string GenerateJSONWebToken(User userInfo)
        //{
        //    var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config["Jwt:Key"]));
        //    var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);


        //    var tokenHandler = new JwtSecurityTokenHandler();
        //    var tokenDescriptor = new SecurityTokenDescriptor
        //    {
        //        Subject = new ClaimsIdentity(new Claim[]
        //        {
        //            new Claim(ClaimTypes.NameIdentifier, userInfo.ToString()),
        //        }),
        //        Expires = DateTime.UtcNow.AddDays(7),
        //        Issuer = _config["Jwt:Issuer"],
        //        Audience = _config["Jwt:Audience"],
        //        SigningCredentials = credentials
        //    };

        //    var token = tokenHandler.CreateToken(tokenDescriptor);
        //    return tokenHandler.WriteToken(token);
        //}

        public async Task<string> GenerateJSONWebToken()
        {
            Func<string> func = () =>
            {
                var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config["Jwt:Key"]));
                var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);

                var tokenHandler = new JwtSecurityTokenHandler();
                var tokenDescriptor = new SecurityTokenDescriptor
                {
                    Subject = null,
                    Expires = DateTime.UtcNow.AddDays(7),
                    Issuer = _config["Jwt:Issuer"],
                    Audience = _config["Jwt:Audience"],
                    SigningCredentials = credentials
                };

                var token = tokenHandler.CreateToken(tokenDescriptor);
                return tokenHandler.WriteToken(token);
            };

            Task<string> genToken = new Task<string>(func);
            genToken.Start();
            await genToken;
            return genToken.Result;
        }
    }
}

