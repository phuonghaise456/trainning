using System;
using AutoMapper;
using TrainingMVC.Dtos;
using TrainingMVC.Models;

namespace IAuthenticationService
{
    public class AutoMappingProfile : Profile
    {
        public AutoMappingProfile()
        {
            CreateMap<Book, BookDto>();
            CreateMap<BookDto, Book>();
            CreateMap<CategoryDto, Category>();
            CreateMap<Category, CategoryDto>();
        }
    }
}
