using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;

namespace Trainning.Middleware
{
    public class CheckUrlMiddeware
    {
        private readonly RequestDelegate _next;
        public CheckUrlMiddeware(RequestDelegate requestDelegate)
        {
            _next = requestDelegate;
        }

        public async Task InvokeAsync(HttpContext httpContext)
        {
            //if (httpContext.Request.Path.ToString().Contains("/swagger"))
            //{
            //    httpContext.Response.Headers.Add("Urlvalid", "Valid");
            //    await _next(httpContext);
            //}
            //else
            //{
            //    await Task.Run(async () =>
            //    {
            //        string html = "<h1>Invalid URL</h1>";
            //        httpContext.Response.StatusCode = StatusCodes.Status403Forbidden;
            //        await httpContext.Response.WriteAsync(html);
            //    });
            //}
            await _next(httpContext);
        }
    }

    public static class CheckUrlMiddewareExtensions
    {
        public static IApplicationBuilder UseCheckUrl(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<CheckUrlMiddeware>();
        }
    }
}
