using Training.Dtos;
using System.Collections.Generic;
using System.Threading.Tasks;
using Training.Models;

namespace Training.Services
{
    public interface ICategoryService
    {
        Task<List<CategoryDto>> GetAll();
        Task<List<CategoryWithBooksDto>> GetCategoryWithBooks();
    }
}
