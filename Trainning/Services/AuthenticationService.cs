using System;
using Trainning.Models;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;

namespace Trainning.Services
{
    public class AuthenticationService : IAuthenticationService
    {
        /// IConfiguration define in appsetting.json
        private IConfiguration _config;

        public AuthenticationService(IConfiguration config)
        {
            _config = config;
        }

        //public async Task<User> AuthenticationUser(string userName, string password)
        //{
        //    var user = _listUser.SingleOrDefault(u => u.UserName == userName && u.Password == password);
        //    return user;
        //}

        //public string GenerateJSONWebToken(User userInfo)
        //{
        //    var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config["Jwt:Key"]));
        //    var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);


        //    var tokenHandler = new JwtSecurityTokenHandler();
        //    var tokenDescriptor = new SecurityTokenDescriptor
        //    {
        //        Subject = new ClaimsIdentity(new Claim[]
        //        {
        //            new Claim(ClaimTypes.NameIdentifier, userInfo.ToString()),
        //        }),
        //        Expires = DateTime.UtcNow.AddDays(7),
        //        Issuer = _config["Jwt:Issuer"],
        //        Audience = _config["Jwt:Audience"],
        //        SigningCredentials = credentials
        //    };

        //    var token = tokenHandler.CreateToken(tokenDescriptor);
        //    return tokenHandler.WriteToken(token);
        //}

        //public async Task<string> GenerateJSONWebToken()
        //{
        //    Func<string> func = () =>
        //    {
        //        var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config["Jwt:Key"]));
        //        var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);


        //        var tokenHandler = new JwtSecurityTokenHandler();
        //        var tokenDescriptor = new SecurityTokenDescriptor
        //        {
        //            Subject = null,
        //            Expires = DateTime.UtcNow.AddDays(7),
        //            Issuer = _config["Jwt:Issuer"],
        //            Audience = _config["Jwt:Audience"],
        //            SigningCredentials = credentials
        //        };

        //        var token = tokenHandler.CreateToken(tokenDescriptor);
        //        return tokenHandler.WriteToken(token);
        //    };

        //    Task<string> genToken = new Task<string>(func);
        //    genToken.Start();
        //    await genToken;
        //    return genToken.Result;
        //}
    }
}

