using System.Collections.Generic;
using System.Threading.Tasks;
using MongoDB.Driver;
using AutoMapper;
using System;
using Training.Models;
using System.Net.Http;
using Training.Dtos;
using System.Linq;

namespace Training.Services
{
    public class CategoryService : ICategoryService
    {
        private readonly IMongoCollection<Category> _categories;
        private readonly IMongoCollection<Book> _books;

        private readonly IMapper _mapper;

        public CategoryService(DbContext dbContext, IMapper mapper)
        {
            _categories = dbContext.Categories;
            _books = dbContext.Books;
            _mapper = mapper;
        }

        public async Task<List<CategoryDto>> GetAll()
        {
            var categoriesDto = new List<CategoryDto>();

            Func<List<Category>> func = () =>
            {
                return _categories.Find<Category>(c => true).ToList();
            };

            Task<List<Category>> getData = new Task<List<Category>>(func);
            getData.Start();
            await getData;

            getData.Result.ForEach(c => categoriesDto.Add(_mapper.Map<CategoryDto>(c)));

            return categoriesDto;
        }

        public async Task<List<CategoryWithBooksDto>> GetCategoryWithBooks()
        {
            var c = _categories.Find<Category>(c => true).ToList();
            var b = _books.Find<Book>(b => true).ToList();

            var query = from p in c.AsQueryable()
                        join o in b.AsQueryable() on p.CategoryName equals o.Category into joinedBook
                        select new CategoryWithBooks()
                        {
                            Id = p.Id,
                            CategoryName = p.CategoryName,
                            Snippet = p.Snippet,
                            Books = joinedBook
                        };

            var categories = _mapper.Map<List<CategoryWithBooksDto>>(query.ToList());
            return categories;
        }
    }
}
