using System;
using System.Collections.Generic;
using MongoDB.Driver;
using Training.Models;
using System.Linq;
using System.Threading.Tasks;
using Training.Dtos;
using AutoMapper;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;

namespace Training.Services
{
    public class BookService : IBookService
    {

        private readonly IMongoCollection<Book> _books;
        private readonly IMapper _mapper;

        public BookService(IConfiguration settings, IMapper mapper, IOptions<BookstoreDatabaseSettings> option, DbContext dbContext)
        {

            //var client = new MongoClient(settings.ConnectionString);
            //var database = client.GetDatabase(settings.DatabaseName);
            //_books = database.GetCollection<Book>(settings.BooksCollectionName);

            _books = dbContext.Books;

            _mapper = mapper;
        }

        public async Task<Object> GetAll(int page, int pageSize)
        {
            //var booksDto = new List<BookDto>();

            //Func<List<Book>> func = () =>
            //{
            //    return _books.Find<Book>(b => true).ToList();
            //};

            //Task<List<Book>> getData = new Task<List<Book>>(func);
            //getData.Start();
            //await getData;

            //getData.Result.ForEach(b => booksDto.Add(_mapper.Map<BookDto>(b)));

            //return booksDto;
            var results = await QueryByPage(page, pageSize, _books);

            var booksDto = new List<BookDto>();
            results.readOnlyList.ToList().ForEach(b => booksDto.Add(_mapper.Map<BookDto>(b)));

            var obj = new
            {
                totalPages = results.totalPages,
                books = booksDto
            };
            return obj;
        }

        public async Task<BookDto> GetById(string id)
        {
            Func<object, Book> myfunc = (object obj) =>
            {
                string id = (string)obj;
                return _books.Find(b => b.Id == id).FirstOrDefault();
            };

            Task<Book> getData = new Task<Book>(myfunc, id);

            getData.Start();
            await getData;

            return _mapper.Map<BookDto>(getData.Result);
        }

        public async Task Add(BookDto bookIn)
        {
            Action<object> act = (object obj) =>
            {
                var bookDto = (BookDto)obj;
                _books.InsertOne(_mapper.Map<Book>(bookDto));
            };
            Task addData = new Task(act, bookIn);
            addData.Start();
            await addData;
        }

        public async Task Update(string id, BookDto bookIn)
        {
            Action<object> act = (object obj) =>
            {
                dynamic arg = obj;
                var book = _mapper.Map<Book>((BookDto)arg.book);
                var id = (string)arg.ID;
                _books.ReplaceOne(b => b.Id == id, book);
            };
            Task updateData = new Task(act, new { ID = id, book = bookIn });
            updateData.Start();
            await updateData;
        }

        public async Task Remove(BookDto bookIn)
        {
            Action<object> act = (object obj) =>
            {
                var book = _mapper.Map<Book>((BookDto)obj);
                _books.DeleteOne(b => b.Id == book.Id);
            };
            Task removeData = new Task(act, bookIn);
            removeData.Start();
            await removeData;
        }

        public async Task Remove(string id)
        {
            Action<object> act = (object obj) =>
            {
                var id = (string)obj;
                _books.DeleteOne(b => b.Id == id);
            };
            Task removeData = new Task(act, id);
            removeData.Start();
            await removeData;
        }

        

        private static async Task<(int totalPages, IReadOnlyList<Book> readOnlyList)> QueryByPage(int page, int pageSize, IMongoCollection<Book> collection)
        {
            var countFacet = AggregateFacet.Create("count",
                PipelineDefinition<Book, AggregateCountResult>.Create(new[]
                {
                PipelineStageDefinitionBuilder.Count<Book>()
                }));

            var dataFacet = AggregateFacet.Create("data",
                PipelineDefinition<Book, Book>.Create(new[]
                {
                PipelineStageDefinitionBuilder.Sort(Builders<Book>.Sort.Ascending(x => x.BookName)),
                PipelineStageDefinitionBuilder.Skip<Book>((page - 1) * pageSize),
                PipelineStageDefinitionBuilder.Limit<Book>(pageSize),
                }));

            var filter = Builders<Book>.Filter.Empty;
            var aggregation = await collection.Aggregate()
                .Match(filter)
                .Facet(countFacet, dataFacet)
                .ToListAsync();

            var count = aggregation.First()
                .Facets.First(x => x.Name == "count")
                .Output<AggregateCountResult>()
                ?.FirstOrDefault()
                ?.Count ?? 0;

            var totalPages = 0;
            if (((int)count % pageSize) > 0)
            {
                totalPages = (int)count / pageSize;
                totalPages++;
            }
            else
            {
                totalPages = (int)count / pageSize;
            }

            var data = aggregation.First()
                .Facets.First(x => x.Name == "data")
                .Output<Book>();

            return (totalPages, data);
        }
    }
}
