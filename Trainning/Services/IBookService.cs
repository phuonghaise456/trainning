using System;
using Training.Dtos;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace Training.Models
{
    public interface IBookService
    {
        Task<Object> GetAll(int page, int pageSize);

        Task<BookDto> GetById(string id);

        Task Add(BookDto book);

        Task Update(string id, BookDto bookIn);

        Task Remove(BookDto bookIn);

        Task Remove(string id);
    }
}
