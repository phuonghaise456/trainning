using AutoMapper;
using Training.Dtos;
using Training.Models;

namespace Trainning
{
    public class AutoMappingProfile : Profile
    {
        public AutoMappingProfile()
        {
            CreateMap<Book, BookDto>();
            CreateMap<BookDto, Book>();

            CreateMap<CategoryDto, Category>();
            CreateMap<Category, CategoryDto>();

            CreateMap<CategoryWithBooks, CategoryWithBooksDto>();
        }
    }
}
