using System;
using System.ComponentModel.DataAnnotations;
using MongoDB.Bson.Serialization.Attributes;

namespace Trainning.Models
{
    public class User
    {
        [BsonElement]
        public string UserName { get; set; }

        [BsonElement]
        public string Password { get; set; }

        [BsonElement]
        public bool RememberMe { get; set; }
    }
}
