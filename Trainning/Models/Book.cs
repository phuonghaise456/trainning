using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace Training.Models
{
    public class Book
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }

        [BsonElement("Name")]
        public string BookName { get; set; }

        [BsonElement]
        public decimal Price { get; set; }

        [BsonElement]
        public string Snippet { get; set; }

        [BsonElement]
        public string Image { get; set; }

        [BsonElement]
        public string Description { get; set; }

        [BsonElement]
        public string Category { get; set; }

        [BsonElement]
        public string Author { get; set; }

    }
}
