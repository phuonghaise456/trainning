using System;
using System.Collections.Generic;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace Training.Models
{
    public class Category
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }

        [BsonElement("Name")]
        public string CategoryName { get; set; }

        [BsonElement]
        public string Snippet { get; set; }

    }

    public class CategoryWithBooks
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }

        [BsonElement("Name")]
        public string CategoryName { get; set; }

        [BsonElement]
        public string Snippet { get; set; }

        public IEnumerable<Book> Books { get; set; }
    }




}
