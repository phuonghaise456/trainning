using System;
using System.Collections.Generic;
using Training.Models;

namespace Training.Dtos
{
    public class CategoryDto
    {
        public string Id { get; set; }

        public string CategoryName { get; set; }

        public string Snippet { get; set; }
    }

    public class CategoryWithBooksDto
    {
        public string Id { get; set; }

        public string CategoryName { get; set; }

        public string Snippet { get; set; }

        public IEnumerable<Book> Books { get; set; }
    }
}
