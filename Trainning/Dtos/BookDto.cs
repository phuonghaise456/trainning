using System;
namespace Training.Dtos
{
    public class BookDto
    {
        public string Id { get; set; }

        public string BookName { get; set; }

        public decimal Price { get; set; }

        public string Category { get; set; }

        public string Author { get; set; }

        public string Snippet { get; set; }

        public string Image { get; set; }

        public string Description { get; set; }
    }
}
