using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Training.Dtos;
using Training.Models;

namespace Trainning.Controllers
{

    [ApiController]
    [Route("api/[controller]/[action]")]
    public class BooksController : ControllerBase
    {
        private readonly IBookService _bookService;

        public BooksController(IBookService bookService)
        {
            _bookService = bookService;

        }

        //[Authorize("book.read")]
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<ActionResult<IEnumerator<BookDto>>> GetAll(int page, int pageSize)
        {
            var bookDtos = await _bookService.GetAll(page, pageSize);

            return Ok(bookDtos);
        }

        //[Authorize("book.read")]
        [HttpGet("{id}")]
        public async Task<ActionResult<IEnumerator<BookDto>>> GetById(string id)
        {
            var book = await _bookService.GetById(id);

            if (book == null)
            {
                return NotFound();
            }

            return Ok(book);
        }

        //[Authorize("book.write")]
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status201Created)]
        public async Task<ActionResult<BookDto>> Add(BookDto bookIn)
        {
            if (bookIn == null)
            {
                return NotFound();
            }
            await _bookService.Add(bookIn);

            return Ok(new { status = StatusCodes.Status201Created, message = "Add Successfuly" });
        }

        //[Authorize("book.write")]
        [HttpPut("{id}")]
        public async Task<IActionResult> Update(string id, BookDto bookIn)
        {
            var book = await _bookService.GetById(id);

            if (book == null)
            {
                return NotFound();
            }

            await _bookService.Update(id, bookIn);
            return Ok(new { status = StatusCodes.Status200OK, message = "Update Successfuly" });
        }

        //[Authorize("book.delete")]
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(string id)
        {
            var book = await _bookService.GetById(id);
            if (book == null)
            {
                return NotFound();
            }

            await _bookService.Remove(book.Id);

            return Ok(new { status = StatusCodes.Status200OK, message = "Delete Successfuly" });
        }
    }
}