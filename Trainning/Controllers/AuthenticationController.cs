using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Trainning.Models;

namespace Trainning.Controllers
{
    [Produces("application/json")]
    [ApiController]
    [Route("api/[controller]/[action]")]
    public class AuthenticationController : ControllerBase
    {
        private IAuthenticationService _authenService;

        public AuthenticationController(IAuthenticationService authenService)
        {
            _authenService = authenService;
        }

        //[HttpPost]
        //[AllowAnonymous]
        //[ProducesResponseType(StatusCodes.Status202Accepted)]
        //[ProducesResponseType(StatusCodes.Status401Unauthorized)]
        //[ProducesResponseType(StatusCodes.Status204NoContent)]
        //public async Task<IActionResult> Login([FromBody] User user)
        //{
        //    var _user = _authenService.AuthenticationUser(user.UserName, user.Password);

        //    if (_user.Result != null)
        //    {
        //        var tokenString = _authenService.GenerateJSONWebToken(user);
        //        return Ok(new { Token = tokenString, Message = "Success" });

        //    }
        //    return NoContent();
        //}

        //[HttpGet]
        //[AllowAnonymous]
        //public async Task<IActionResult> GetToken()
        //{
        //    var tokenString = await _authenService.GenerateJSONWebToken();
        //    return Ok(new { Token = tokenString, Message = "Success" });
        //}

    }
}
